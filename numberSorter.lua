--[[----------------------------------
--   Bubble Sorter for Numbers v1.0
--   author: Mel Stychen S. Tan
--]]----------------------------------


-------------------
-- Parameters
-------------------

-- EDIT THIS ARRAY TO YOUR LIKING :)
givenArray = {1, 2, 4, 10, 3, 8, 2}



-------------------
-- Functions
-------------------

function printArray(array)
    print(table.concat( array, ", "))
end

function bubbleSort(array)
	
	arrayCount = #array;

	-- check if a one entry array


	done = false

	while not done do
		done = true --check value if really done
		-- iterate array
		for currentIndex = 1, (arrayCount - 1) do
			nextIndex = currentIndex + 1

			-- swap if previous index is greater than current index
			if array[currentIndex] > array[nextIndex] then
				array[currentIndex], array[nextIndex] = array[nextIndex], array[currentIndex];
				done = false;  -- not yet done, there is still was swappable content
			end

		end

	end

	return array;

end


-------------------
-- Main Program
-------------------
print('\n')
print('#################################')
print('#     Number Bubble Sorter      #')
print('#################################')
print('\n\n')

print('Given Array:') 
printArray(givenArray)

resultArray = bubbleSort(givenArray)

print ('Resulting Array:')
printArray(resultArray)
