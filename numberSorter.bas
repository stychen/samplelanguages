'''''''''''''''''''''''''''''''''''''
' Bubble Sorter for Numbers v1.0
' author: Mel Stychen S. Tan
'''''''''''''''''''''''''''''''''''''

'------------'
' Parameters '
'------------'

'''QBASIC SUCKS, CAN NOT DECLARE AND ARRAY WITH IMPLICIT VALUES AND SIZE!


''' ATTENTION!!: INPUT NUMBER OF ITEMS IN YOUR ARRAY HERE!
''' ATTENTION!!: INPUT YOUR ITEMS WITH THEIR RESPECTIVE INDEX, I ONLY ACCEPT INTEGERS
''' ATTENTION!!: Sorry for the trouble, QBASIC IS SUCH A WIMPY LANGUAGE
''' EXAMPLE INPUT BELOW
NUMBER_OF_ITEMS = 7

DIM givenArray(NUMBER_OF_ITEMS) AS INTEGER ''' WARNING!!!: DO NOT EDIT ME!

givenArray(0) = 1
givenArray(1) = 2
givenArray(2) = 4
givenArray(3) = 10
givenArray(4) = 3
givenArray(5) = 8
givenArray(6) = 2

''' ANOTHER SAMPLE INPUT
''' NUMBER_OF_ITEMS = 4
''' givenArray(0) = 10
''' givenArray(1) = 20
''' givenArray(2) = 13
''' givenArray(3) = 17




''' !!! NO EDITING BELOW !!!

'--------------'
' Main Program '
'--------------'

'''PRINT GIVEN ARRAY

PRINT "Given Array:"
FOR arrayIndex = 0 TO NUMBER_OF_ITEMS - 1
	PRINT givenArray(arrayIndex),
NEXT arrayIndex


'''PERFORM BUBBLE SORT

DO
  DONE = 1
  FOR curIndex = 1 TO NUMBER_OF_ITEMS - 1
  	prevIndex = curIndex - 1 
    IF givenArray(prevIndex) > givenArray(curIndex) THEN
      DONE = 0
      SWAP givenArray(curIndex), givenArray(prevIndex)
    END IF
  NEXT curIndex
LOOP UNTIL DONE


'''PRINT RESULT ARRAY

PRINT "Resulting Array:"
FOR arrayIndex = 0 TO NUMBER_OF_ITEMS - 1
	PRINT givenArray(arrayIndex),
NEXT arrayIndex