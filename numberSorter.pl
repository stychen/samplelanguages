#--------------------------------#
# Bubble Sorter for Numbers v1.0
# author: Mel Stychen S. Tan
#--------------------------------#

use feature ':5.10'; # use Perl 5.10

use strict;
use warnings;


##############
# Parameters #
##############

# EDIT THIS ARRAY TO YOUR LIKING :)
my @given_array = (1, 2, 4, 10, 3, 8, 2);



################
# Sub-routines #
################

sub say_array {
	my @array = @_;
	say join(", ", @array);
}

sub bubble_sort {
	my @array = @_;
	my $done = 0;

	while (not $done) {

		$done = 1; #check value if okay

		#iterate array
		for my $cur_index ( 1 .. $#array) {  
			my $prev_index = $cur_index - 1;

			# swap if previous index is greater than current index
			if ( $array[$prev_index] > $array[$cur_index]) {
				($array[$prev_index], $array[$cur_index]) = ($array[$cur_index], $array[$prev_index]);

				$done = 0;  # not yet done, there is still was swappable content
			}
		}
	}

	return @array;
}


###############
# Main Script #
###############

say 
"
#################################
#      NUMBER BUBBLE SORTER     #
#################################


";


say "Given Array:";
say_array (@given_array);

my @resulting_array = bubble_sort @given_array;

say "Resulting Array:";
say_array (@resulting_array);

exit 1